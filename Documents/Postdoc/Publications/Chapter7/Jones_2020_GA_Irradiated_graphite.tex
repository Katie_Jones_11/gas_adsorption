\documentclass[a4paper,british,review]{elsarticle}
\usepackage[LGR,T1]{fontenc}
\usepackage[british]{babel}
\usepackage[latin9]{inputenc}
\usepackage{array}
\usepackage{rotfloat}
\usepackage{textcomp}
\usepackage{amssymb} 
\usepackage{amsmath} % to align equations
\usepackage{mathrsfs} % curly font in math, called using mathscr
\usepackage{graphicx}
\usepackage{subcaption} % to have two images under one caption
\usepackage{subscript}
\usepackage{gensymb} %for degree symbol
\usepackage{todonotes}
\usepackage{xargs}  % Use more than one optional parameter in a new commands
\renewcommand{\baselinestretch}{1.5}


\newcommandx{\Peter}[2][1=]{\todo[linecolor=red,backgroundcolor=red!25,bordercolor=red,#1]{#2}}
\newcommandx{\Katie}[2][1=]{\todo[linecolor=blue,backgroundcolor=blue!25,bordercolor=blue,#1]{#2}}
\newcommandx{\Maurizio}[2][1=]{\todo[linecolor=green,backgroundcolor=green!25,bordercolor=green,#1]{#2}}
\newcommandx{\thiswillnotshow}[2][1=]{\todo[disable,#1]{#2}}

\usepackage{mhchem}
\usepackage{rotating}
\biboptions{numbers,sort&compress}
\makeatletter

\usepackage{mathtools}
\newcommand\filledcirc{{\color{blue}\bullet}\mathllap{\circ}}


\hyphenpenalty=1000 \emergencystretch=5em

\begin{document}

\title{The application of krypton gas adsorption to characterise the micro/mesoscale void space of nuclear graphite and the impact of irradiation damage}


\author[plym]{Katie L. Jones}
\author[plym]{G. Peter Matthews}
\author[plym]{Giuliano M. Laudone\corref{cor1}}
\ead{glaudone@plymouth.ac.uk}

\cortext[cor1]{Corresponding author.}


\address[plym]{Faculty of Science and Engineering, University of Plymouth, Plymouth, UK.}

\begin{abstract}

This work presents the application of gas adsorption analysis, namely krypton adsorption, for the surface characterisation of graphitic samples. 


\end{abstract}
\maketitle

\section{Introduction\label{sec:Introduction}}

Gas adsorption is a widely used technique for quantifying the total surface areas and pore size distribution of materials. 
The physical adsorption of gases on a solid surface will exhibit different behaviours dependant on the samples microstructure, operational temperatures and gases used to probe the material. 
Such differences are reflected in the shape of the adsorption isotherm which, with careful interpretation, can reveal information about the samples morphology \cite{Horikawa2011}. 
For porous materials, the size of pores in the adsorbent have a significant affect of the adsorption process but also the surface heterogeneity of the adsorbent, such as the geometric or energetic characteristics, will largely affect the adsorbate-adsorbent interactions, and consequently the formation of the isotherm \cite{Thommes2015}.

 
 The usefulness of Kr adsorption is often reported as limited by the lack of available models for the interpretation of the experimental results in terms of pore size information \cite{Thommes2015}.


 Therefore a bespoke model, introduced in section \ref{GCMCINTERP}, was created to obtain void size information.
 
 In this work, simulated adsorption isotherms of Kr onto graphite in slit-shaped pores composed of two uniform graphite slabs are generated with a Grand Canonical Monte Carlo method developed for this study \cite{Laudone2018}. 
 The advantage of using the GCMC approach over more traditional calculations of PSD, such as the BJH method, is that one single method is able to investigate the distribution of pore sizes in the micro-, meso-porous range, from 0.5 nm to 150 nm. 
 An alternative technique to gas adsorption (e.g. mercury porosimetry) must be used to assess the largest pore features \cite{Rouquerol1994}.
 

Graphite occurs in two distinct forms, natural and synthetic (artificial) graphite.
Natural graphite is classified as a mineral which consists of graphitic carbon whereas, a synthetic graphite is defined under the broader class, which can range from a ?single crystal? graphite such as Highly Orientated Pyrolytic Graphite (HOPG) to polygranular graphites, which are comprised of individual microscopic grains. 
Nuclear graphites are a sub-class of the latter.
In graphite, the PSDs refer to the number and size of voids within the bulk phase, as well as the inter-connections between them which are the result of the packing of crystallites and particles within the structure \cite{Nightingale1962_25}.
The percentage porosity for different grades of virgin nuclear graphite can range between 20\% to 30\%, compared to only <1\% for a single graphite crystal \cite{ElGenk2012}. 
Synthetic graphites have long been incorporated into a variety of nuclear reactor designs, from the very first CP-1 experimental reactor built in 1942, to the old style Magnox and RBMK reactors \cite{Nightingale1962_25}, through to the advanced gas-cooled reactors currently operational in the UK.
Due to its mechanical and thermal properties, combined with its behaviour as a neutron moderator and reflector, nuclear-grade graphite is also being used as a moderator and structural core material in Generation-IV reactor designs, such as the high-temperature gas-cooled reactors (HTGR) and molten salt reactors \cite{Allen2010,Murty2008}. 

In order to adapt to different reactor designs, numerous grades of nuclear graphite have been developed worldwide. 
It is therefore not uncommon for nuclear graphites to display dramatically different mechanical and physical properties. 
For example, some display anisotropic properties and others near-isotropic. 
The directional dependence is determined by the types of filler used, the manufacturing processes, or a combination of both \cite{HALL2006_17,FREEMAN2016}. 
Great differences can be seen in the graphites from the very first CP1 reactor (e.g. AGOT graphite) to those used in the Magnox reactors (e.g. Pile Grade A, PGA) and even more so in those used in the current AGRs (e.g. Gilsocarbon), which are discussed further in section \ref{sub:2-Microstructureproperties}.  
Gilsocarbon was developed specifically for use in the AGRs \cite{KELLY1992_19}.
The filler particles incorporated are spherical in nature. 
It is the coke filler particles that gives Gilsocarbon its unique spherical, onion-like cracks (Figure \ref{fig:2-GILSO1}). 
Whilst the crystallites within the particles align circumferentially and demonstrate anisotropic physical properties, the bulk material, which is isomolded, shows near-isotropic characteristics \cite{Bradford2014}.


During standard reactor operating conditions, the nuclear graphite core undergoes a plethora of changes which vastly affect the graphites microstructure, brought about by the irradiative environment.
As a consequence, the electronic, mechanical and structural properties of graphite are also altered \cite{Kelly1982,KRISHNA20152,NEIGHBOUR2001}. 
It is well understood that bulk graphite shrinkage is brought about as a direct consequence of irradiation damage which drives the expansion of individual crystallites in the c-direction but whose contribution is taken up by accommodating porosity. 
Sequentially, the corresponding shrinkage along the a, b axes drives the overall dimensional change, until all the accommodation porosity is filled and dimensional turnaround is observed, where the bulk graphite begins to swell with increased dose \cite{Marsden2016,Neighbour2000}. 
The dimensional effects will alter the internal pore geometry and different graphite grades, which comprise different pore geometries, will in turn be effected proportionately \cite{Minshall1996}. 
It is proposed that, whilst dimensional changes may leave the open pore volumes unchanged, the accessible internal surface area would be altered.
Additionally, in the presence of irradiation, the CO$_{2}$ reacts to form volatile species that corrode the gas accessible surface of the graphite. 
This causes significant weight loss of the graphite, and hence the development of the pore volume, as well as, geometrical changes to the graphite void space \cite{Payne2015}. 

The increased presence of porosity throughout the structure, caused by oxidative and irradiative process, will increase the internal surface area of the graphite, which could reach values of 1 m$^{2}$g$^{-1}$ in high mass loss material.
This value is still very small compared to many other samples analysed using this technique and therefore required the application of a highly sensitive  approach in order to accurately characterise the changes in surface area.





\section{Experimental}
\subsection{Materials}

This study focused on the examination of varying grade of virgin, irradiated and oxidised nuclear graphites. 
However, when applicable, other graphitic material has been studied to support current hypotheses or attempt to clarify uncertainties in the techniques presented in the chapter. 

Significant milestones in the requirements, and consequently, the development of nuclear graphites have been reached in recent years, due to increased knowledge of their structure and behaviour during operational conditions \cite{Trevethan2016,Marsden2016,Berre2006,Gao2016,Hutcheon1966}.  
Some examples of other nuclear grade graphites, both old and new, are briefly described below.
The standard manufacturer's information, when available, has been tabulated in Table \ref{table:3-ManufacProps}.

\paragraph{PGA}: Pile grade A (PGA) graphite was manufactured for use in early Magnox gas-cooled reactors.
It comprised filler particles derived from the petroleum industry \cite{KELLY1992_19}. 
The final particle arrangement in PGA (visible in Figure\ref{fig:2-PGA1}) consisted of elongated, needle-like shape particles which were preferentially aligned, along the extrusion axis, which resulted in a bulk material with anisotropic properties. 
This was amplified by the manufacturing process as PGA was formed via extrusion which caused the longitudinal axis of the needle coke to align with the
extrusion direction (with-grain). \\

\paragraph{PCEA}: is a purified grade of graphite, produced for the nuclear industry by GrafTech International Ltd, suitable for a wide range of nuclear applications. 
Design specifications ensured a strong, thermally conductive material \cite{PatentPCEA} where efforts were made to increase the density via multiple impregnation steps in order to increase the resistance of the material against creep and oxidative processes.
It is currently under consideration as a candidate for high-dose regions of the new generation nuclear plants, such as the prismatic core reactors.  \\

\paragraph{IG-110}: is a fine-grained, isotropic graphite which exhibits high thermal durability and strength, making it highly suitable for use in the nuclear industry \cite{Jones2018}. 
At the present time, IG-110 is used in two of the existing HTGR cores as a moderator, namely the Japanese High Temperature engineering Test Reactor (HTTR) \cite{YAN2013} and the Chinese High Temperature Reactor-Pebble bed Modules (HTR-10) \cite{Zhen2014}.\\

\paragraph{IG-430}: is comparable to IG-110, and was developed by the same manufacturers, Toyo Tanso Ltd., Japan.
It was designed to display higher bulk density, strength and thermal conductivity when compared with IG-110, where the improved physical characteristics make it a suitable candidate for use in Generation-IV reactors.\\

\paragraph{NBG-18}: is a coarse grained graphite, produced by SGL Carbon Group, Germany for use in the nuclear industry. 
It contains large spherical filler particles which give rise to its semi-isotropic nature. 
NBG-18 is a top candidate for use in the high-dose regions of new generation nuclear plants, such as a reflector graphite pebble bed reactors or for use in the very high temperature reactors.\\

\begin{table}[htbp]
	\caption{Manufacturer's characterisation of the graphite types.}
	\label{table:3-ManufacProps}
	\begin{adjustwidth}{-2cm}{-2cm}
		\centering
		\setlength{\extrarowheight}{5pt}
		\scalebox{0.85}{
			\rotatebox{90}{
				\begin{tabular}{c|c| c| c c c c} 
					& Coke & Forming & Coke filler& Bulk & Tensile & Compressive\\ 
					Sample& derived & process & particle & Density & strength & strength \\
					& from & & size ($\mu$m) & (g cm$^{-3}$) & (MPa) & (MPa)  \\ [1ex]
					\hline
					HOPG (GRADE ZYA) \cite{PatentHOPG} & - & & 30-40 & 2.265 & 80 & 100\\
					\hline
					Gilsocarbon \cite{PatentGilso}& Petroleum (Gilsonite) & Isomolded &1 - 300 & 1.81 & 31.7 & 89.6\\
					IG-110 \cite{Tanso1997} & Petroleum & Isomolded & 10 & 1.77 & 25 & 78 \\
					IG-430 \cite{Tanso1997} & Pitch & Isomolded & 10 & 1.82 & 37 & 90 \\
					PGA \cite{IAEA2006} & Petroleum & Extruded & 1-800 & 1.74 & 17* 11**& 27* 27** \\
					PCEA \cite{PatentPCEA} & Petroleum & Extruded & 360 - 800 & 1.84 & 17 - 22 & 61 -68 \\
					NBG-18  & Pitch & Vibration moulded & 1 - 300 & 1.85 & 24 & 77 \\ [1ex]
					\hline
					\multicolumn{7}{c}{* Parallel to extrusion ** Perpendicular to extrusion}\\
				\end{tabular}
			}
		}
	\end{adjustwidth}
\end{table}


\subsection{Sample preparation \label{Sampleprep}}

The IG-graphites were machined to cylinders of the same diameter as the Gilsocarbon samples using a tungsten carbide tool on a standard lathe. 
The samples were then cut to approximately 5 mm in length with a diamond saw.
The Gilsocarbon samples were already prepared and required no further machining.
Samples were cleaned via the same sampling protocol detailed in chapter \ref{sub:5-IG-Sample-preparation}.
Prior to analysis, samples were dried under vacuum for a minimum of 3 hours at 305 $^{\circ}$C in order to remove residual moisture using the BELPREP-vac (MicrotracBEL, Japan). 
The drying procedure was repeated in between each experimental measurement to ensure a moisture free surface.

\subsection{Gas adsorption \label{GasAdsorption}}
\paragraph{Instrument}
Low pressure gas adsorption was undertaken using a BELSORP-max volumetric gas adsorption instrument (MicrotracBEL, Japan). Information on the experimental system can be found in chapter \ref{sub:5-GA-instrument-setup}. 
Throughout this body of work, gas adsorption measurements were performed on port 3 of the instrument, shown in Figure \ref{fig:Port3regular}, which is equipped with a highly sensitive pressure transducer which detects pressure changes as small as 0.0133 kPa. 
By utilizing this set-up high resolution measurements could be obtained over extremely low relative pressure regions. 
The BELSORP series employs volumetric theory to measure adsorption, where 
adsorption measurements, performed at constant temperature, quantify the volume of gas adsorbed (adsorbate) by a solid or powdered material as a function of pressure \cite{Van1965}. 
The raw data produces adsorption isotherms, whose shape provides quantitative and qualitative information about the adsorbent's structure.
Adsorption data was automatically recorded using the BEL-Master analysis software.

\begin{figure}[H]
	\centering
	\subfloat[]
	{\label{fig:7-port3}
		\includegraphics[width=0.5\linewidth]{./images/chapter7/Regular_Sample_setup.png}}
	\\
	\subfloat[]
	{\label{fig:7-port3zoom}
		\includegraphics[width=0.5\linewidth]{./images/chapter7/Sample_Reg_cell2.JPG}}
	\caption[Image of the regular sized sample cells on the BELSORP-Max]{Images of a regular sample cell on the high precision port 3 on the BELSORP-Max.
		Image (a) shows both a regular sized blank and sample chamber; Image (b) shows a close up view of the regular sized sample chamber containing the Gilsocarbon sample M281.}
	\label{fig:Port3regular}
\end{figure}

\paragraph{Adsorbates}
During this study several adsorptive options were investigated. 
The initial choice was nitrogen, as many standard interpretation methods exist for graphitised carbons. 
It is possible to determine absolute surface areas using highly accurate volumetric adsorption equipment as low as approximately 0.5 - 1 m$^{2}$g $^{-1}$ with nitrogen as the adsorptive.
Both surface area and the corresponding pore size information can then be calculated, via application of the commercially available software BELMaster, which is supplied with the instrument.
%ADD MORE DETAIL ABOUT SOFTWARE 
However, results presented in section \ref{N-Results} demonstrated the need for an alternative adsorptive gas to be sourced. 
This was due to inconsistencies between replicate isotherms which were thought to be brought about by the limited sample volumes, as well as the low specific surface areas requiring the instrument to operate at its limit of detection.

Krypton was used as an alternative adsorbate due to its capacity to extend the detection limit of the absolute surface areas down to 0.05 m$^{2}$ g$^{-1}$. 
Due to its extremely low saturated vapour pressure, the number of krypton molecules in the free space of the sample cell are significantly lower when compared to nitrogen or argon at their respective boiling temperatures. 
This leads to the high sensitivity of krypton adsorption at 77.35 K, making it the adsorbate of choice for measuring low surface area solids \cite{Beebe1945}.
High precision krypton adsorption isotherms were collected at 77.35 K for the graphitic samples. 
Stable temperatures were achieved by fully submerging both a blank and sample chamber in a 3 L Dewar, filled with liquid nitrogen. 
At 77.35 K, krypton is approximately 38 K below its triple point (TP = 115.8 K). 
It is commonly accepted throughout the literature that the saturated vapour pressure, $p_{0}$, of supercooled liquid krypton can be adopted for the determination of specific surface areas, where the adsorbed krypton layer is in a liquid-like state.
At pressures close to 0.22 kPa krypton begins to sublimate \cite{Thommes2015}.
Measurements were therefore recorded over the relative pressure range: 10$^{-8}<p/p\textsubscript{0} \leqslant$ 0.65, where $p$ is the pressure of the adsorbing gas in kPa, and $p\textsubscript{0}$ is 0.3310 kPa. 

Lastly, CO$_{2}$ adsorption experiments at 273.15 K and 298.15 were performed on a limited number of samples to investigate the presence of microporosity in the graphite structures.  


\paragraph{Interpretation}
Total specific surface areas ($S$\textsubscript{BET}) were calculated via the standard Brunauer-Emmett-Teller (BET) model \cite{Brunauer1938_34}, which incorporates multi-layer adsorption. 
A relative pressure range 0.05 - 0.3 was utilized to deduce the surface area, as at higher values of $p/p_{0}$ capillary condensation effects alter the linearity of the BET plot. 


A method for interpreting the void size information for krypton on a smooth graphite surface, modelled with slit geometry was developed during this study as commercial void size interpretations for krypton adsorption were unavailable. 
The GCMC model, described in section \ref{sub:3-Gasadsorptionmodifications}, was first validated for GCB, and then demonstrated on the IG-graphites and an activated carbon sample. 
The simulation was then applied to the Gilsocarbon graphite samples to provide PSDs that covered the micropore/mesopore range. 
Results for all samples are presented below.


\subsection{Sample Preparation} 

\section{Results}

However, for the irradiated graphite specimens, similarly to the activated carbon sample, there is a noticeable difference between the simulated, stepwise isotherms and the experimental, smoother Kr adsorption isotherms observed over the entire pressure range.
The reasoning for the difference in the shape of the isotherms is discussed in detail in section \ref{IrrGraphIso}; it is likely that the non-uniformities in the graphite surface, introduced most likely by irradiation damage, led to the production of a smoother, non-stepped adsorption isotherm, which cannot be reproduced by the GCMC Kr model. 
In order for the simulation to produce model isotherms calculated by the GCMC  Kr simulation to display a reduction in the stepwise formation of the isotherms, the current Kr kernel would need to be heavily modified, taking in to account the roughness of the graphite's surface.
Due to the time constraints of the project, these amendments could not be completed within the time scale, and instead are discussed in the \textit{future work} section of this thesis \ref{InvestigateSteps}. 

It can be seen in Figures \ref{fig:GCMCM899} - \ref{fig:GCMC3M44} that when fitting the experimental data of the irradiated graphite specimens the simulation again overestimates the initial gas adsorption step at low pressure; where this difference is particularly noticeable in low weight loss specimens due to the very sloped first step.
The simulated curve then progresses horizontally until it intersects the experimental curve, where the fit of the data improves. 
The match between the model and experimental isotherm at the point of the second step causes a further deviation in the data, although the difference between the two models is small.
Additionally, the simulation fails to predict the onset of the third step, at pressures larger than 170 Pa, due to the capillary condensation effect discussed previously.

For the samples with higher weight loss, such as samples 0M55, 3M44 and M595, the distance between the two curves is improved, where the simulation and the experimental isotherms correctly predict the first step observed at low pressure.
However, as the experimental isotherms do not show the formation of the second and third step, and consequently the simulated curve sits either side of the experimental isotherm as it progresses.
In the highest weight loss sample, M595, the GCMC Kr model is able to closely simulate the adsorption isotherm over the entire range of pressures. 

The overall distribution of the pores predicted by the GCMC Kr are similar for the entire irradiated graphite specimens. 
However, the volume contribution of the smallest pores increases in the higher mass loss specimens highlighted by the increasing total cumulative volume plots.
This suggests that in samples of increased mass loss and higher cumulative dose the presence of the smallest pores is increased, likely introduced by radiolytic oxidation of the surface.
However, this contribution of pore volume for the pore range 2 - 50 nm is still very small when compared to the total pore volume measured by micropycnometry.

It should be noted that various kinds of microscopy techniques, such as SEM, can be used to access the porosity at these length scales. 
However, these techniques can only examine a very limited field of view, of what is a very disordered material. 
Gas adsorption is capable of probing the entire gas accessible pore space in one measurement. 
Whilst it cannot reveal information on the closed pore volume, such volumes cannot contribute to gas flow path of the material, and therefore should not impact the pore network model which will be used to access the permeability of the oxidised graphites during Chapter \ref{chapter8}.


\section{Discussion}



\section{Conclusions}


The deductions within this work are semi-quantitative.
In further work, computational modelling will be used to generate quantitative void structure models at every oxidation stage.  
Those will be used to make a definitive judgement on the RPV model, and to provide data for inputting to current larger scale models of reactor core behaviour such as FEAT-DIFFUSE.

\clearpage
 
\section*{Acknowledgements}

The financial support and advice of EDF Energy (Barnwood, Gloucester, UK) is gratefully acknowledged. However, the views expressed in this paper are those of the authors and do not necessarily represent the views of the sponsors. 


\section*{Appendix A. Supplementary materials }

Supplementary data associated with this article can be found in the on-line version at ...

\section*{References}

\bibliographystyle{elsarticle-num-names}
\bibliography{Jones_2020_GA_Irradiated_graphite}

\end{document}
