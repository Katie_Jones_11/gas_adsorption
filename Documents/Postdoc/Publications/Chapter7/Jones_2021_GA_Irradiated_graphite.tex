\documentclass[a4paper,british,review]{elsarticle}

\usepackage[LGR,T1]{fontenc}
\usepackage[british]{babel}
\usepackage[latin9]{inputenc}
\usepackage{array}
\usepackage{rotfloat}
\usepackage{textcomp}
\usepackage{amssymb} 
\usepackage{amsmath} % to align equations
\usepackage{mathrsfs} % curly font in math, called using mathscr
\usepackage{graphicx}
\usepackage{adjcalc}
\usepackage{trimclip}
\usepackage{adjustbox}
\usepackage{subfig}
%\usepackage{subcaption} % to have two images under one caption
\usepackage{subscript}
\usepackage{gensymb} %for degree symbol
\usepackage{todonotes}
\usepackage{xargs}  % Use more than one optional parameter in a new commands
\renewcommand{\baselinestretch}{1.5}


\newcommandx{\Peter}[2][1=]{\todo[linecolor=red,backgroundcolor=red!25,bordercolor=red,#1]{#2}}
\newcommandx{\Katie}[2][1=]{\todo[linecolor=blue,backgroundcolor=blue!25,bordercolor=blue,#1]{#2}}
\newcommandx{\Maurizio}[2][1=]{\todo[linecolor=green,backgroundcolor=green!25,bordercolor=green,#1]{#2}}
\newcommandx{\thiswillnotshow}[2][1=]{\todo[disable,#1]{#2}}

\usepackage{mhchem}
\usepackage{rotating}
\biboptions{numbers,sort&compress}
\makeatletter

\usepackage{mathtools}
\newcommand\filledcirc{{\color{blue}\bullet}\mathllap{\circ}}


\hyphenpenalty=1000 \emergencystretch=5em

\begin{document}

\title{The application of krypton gas adsorption to characterise the micro/mesoscale void space of nuclear graphite and the impact of irradiation damage}


\author[plym]{Katie L. Jones}
\author[plym]{G. Peter Matthews}
\author[plym]{Giuliano M. Laudone\corref{cor1}}
\ead{glaudone@plymouth.ac.uk}

\cortext[cor1]{Corresponding author.}


\address[plym]{Faculty of Science and Engineering, University of Plymouth, Plymouth, UK.}

\begin{abstract}

This work presents the application of gas adsorption analysis, namely krypton adsorption, for the surface characterisation of graphitic samples. 


\end{abstract}
\maketitle

\section{Introduction\label{sec:Introduction}}

Synthetic graphites have long been incorporated into a variety of nuclear reactor designs, from the very first CP-1 experimental reactor built in 1942 through to the advanced gas-cooled reactors currently operational in the UK.
The mechanical and thermal properties of nuclear-grade graphite, combined with its behaviour as a neutron moderator and reflector,continue to see its incorporation in Generation-IV reactor designs, such as the high-temperature gas-cooled reactors (HTGR) and molten salt reactors \cite{Allen2010,Murty2008,WINDES2020}. 
During standard reactor operating conditions, the nuclear graphite core undergoes a plethora of changes which vastly affect the graphites microstructure, brought about by the irradiative environment.
As a consequence, the electronic, mechanical and structural properties of graphite are altered \cite{Kelly1982,KRISHNA20152,NEIGHBOUR2001,Marsden2016,Neighbour2000}. 
It is proposed that, whilst dimensional changes may leave the open pore volume unchanged, the accessible internal surface area would be altered.
Additionally, in some reactor designs CO$_{2}$, in the presence of irradiation, reacts to form volatile species that corrode the gas accessible surface of the graphite. 
This causes significant weight loss of the solid phase, and hence the development of the pore volume, but also alters the geometry and surface roughness of the graphite void space \cite{Payne2015}. 
Characterisation of microstructural features for unirradiated, virgin graphite and the implications of how  by irradiation damage alters the graphite microstructure are essential for the understanding and development of materials used in current and future nuclear reactors.

Standard gas adsorption measurements are widely used for quantification of Specific Surface Areas (SSA) and Pore Size Distributions (PSD) for porous materials. 
The physical adsorption of gases on a solid surface exhibit different behaviours dependant on the samples microstructure, operational temperatures and gases used to probe the material. 
Such differences are reflected in the shape of the adsorption isotherm which, with careful interpretation, can reveal information about the samples morphology \cite{Horikawa2011}. 
For porous materials, the size of pores in the adsorbent have a significant affect of the adsorption process but also the surface heterogeneity of the adsorbent, such as the geometric or energetic characteristics, will largely affect the adsorbate-adsorbent interactions, and consequently the formation of the isotherm \cite{Thommes2015}.

Multilayer adsorption of krypton on highly uniform, non-porous graphitic surfaces has been well studied \cite{Beebe1945, Deboer1967,Larher1974,Ban1987,Takei1998,BENYAHIA2013,Yahia2013} and typically produces a stepped isotherm, classified as a reversible Type VI isotherm by the 2015 IUPAC classification system \cite{Thommes2015}.
The usefulness of Kr adsorption at 77 K is often reported as limited due to the lack of commercial models for the interpretation of the experimental results to generate pore size information\cite{Zukal2006,Thommes2015}.
However, increasing computing power and demand for low surface area materials 
the need for new, more intuitive modelling techniques to obtain pore size information is of huge scientific benefit.
Bespoke models, such as devloped by \citet{Laudone2018} utilise a Grand Canonical Monte Carlo (GCMC) simulation of Kr onto uniform graphite slabs to investigate the distribution of pore sizes from 0.5 nm to 150 nm.
However, in order to provide reliable outputs the model must be able to simulate the experimental isotherms.

This study presents how irradiation damage to the graphites surface presents a unique phenomena which alters the adsorption behaviour of krypton, and hence the shape of the expected isotherm.  




\section{Experimental}
\subsection{Materials}

Significant milestones in the requirements, and consequently, the development of nuclear graphites have been reached in recent years, due to increased knowledge of their structure and behaviour during operational conditions \cite{Trevethan2016,Marsden2016,Berre2006,Gao2016,Hutcheon1966}.
In order to adapt to different reactor designs, numerous grades of nuclear graphite have been developed worldwide. 
It is therefore not uncommon for nuclear graphites to display dramatically different mechanical and physical properties. 
For example, some display anisotropic properties and others near-isotropic. 
The directional dependence is determined by the types of filler used, the manufacturing processes, or a combination of both \cite{HALL2006_17,FREEMAN2016}. 
This study examined the adsorption of Kr at 77 k on a variety of nuclear grade graphites,both virgin and irradiated, and the standard manufacturer's information, when available, for the unirradiated properties of the graphites have been tabulated in Table \ref{table:3-ManufacProps}.
\begin{table}[htbp]
	\caption{Manufacturer's characterisation of the graphite types.}
	\label{table:3-ManufacProps}
		\centering
		\setlength{\extrarowheight}{5pt}
		\scalebox{0.85}{
			\rotatebox{90}{
				\begin{tabular}{c|c| c| c c c c} 
					& Coke & Forming & Coke filler& Bulk & Tensile & Compressive\\ 
					Sample& derived & process & particle & Density & strength & strength \\
					& from & & size ($\mu$m) & (g cm$^{-3}$) & (MPa) & (MPa)  \\ [1ex]
					\hline
					Gilsocarbon \cite{PatentGilso}& Petroleum (Gilsonite) & Isomolded &1 - 300 & 1.81 & 31.7 & 89.6\\
					IG-110 \cite{Tanso1997} & Petroleum & Isomolded & 10 & 1.77 & 25.40 & 78.22 \\
					IG-430 \cite{Tanso1997} & Pitch & Isomolded & 10 & 1.82 & 37 & 90 \\
					NBG-25 &  & Extruded  &  &  &  & \\
					PCEA \cite{PatentPCEA} & Petroleum & Extruded & 360 - 800 & 1.84 & 17 - 22 & 61 -68 \\
					NBG-18  & Pitch & Vibration moulded & 1 - 300 & 1.85 & 24 & 77 \\
					PPEA  &  & Extruded & & 1.85 & 22 & 82 \\[1ex]
					\hline
					\multicolumn{7}{c}{* Parallel to extrusion ** Perpendicular to extrusion}\\
				\end{tabular}
			}
		}
\end{table}


\subsection{Sample preparation \label{Sampleprep}}

The IG-graphites were machined to cylinders of the same diameter as the Gilsocarbon samples using a tungsten carbide tool on a standard lathe. 
The samples were then cut to approximately 5 mm in length with a diamond saw.
The Gilsocarbon samples were already prepared and required no further machining.
Samples were cleaned via the same sampling protocol detailed in chapter \ref{sub:5-IG-Sample-preparation}.
Prior to analysis, samples were dried under vacuum for a minimum of 3 hours at 305 $^{\circ}$C in order to remove residual moisture using the BELPREP-vac (MicrotracBEL, Japan). 
The drying procedure was repeated in between each experimental measurement to ensure a moisture free surface.

\subsection{Gas adsorption \label{GasAdsorption}}
\paragraph{Instrument}
Low pressure gas adsorption was undertaken using a BELSORP-max volumetric gas adsorption instrument (MicrotracBEL, Japan). Information on the experimental system can be found in chapter \ref{sub:5-GA-instrument-setup}. 
Throughout this body of work, gas adsorption measurements were performed on port 3 of the instrument, shown in Figure \ref{fig:Port3regular}, which is equipped with a highly sensitive pressure transducer which detects pressure changes as small as 0.0133 kPa. 
By utilizing this set-up high resolution measurements could be obtained over extremely low relative pressure regions. 
The BELSORP series employs volumetric theory to measure adsorption, where 
adsorption measurements, performed at constant temperature, quantify the volume of gas adsorbed (adsorbate) by a solid or powdered material as a function of pressure \cite{Van1965}. 
The raw data produces adsorption isotherms, whose shape provides quantitative and qualitative information about the adsorbent's structure.
Adsorption data was automatically recorded using the BEL-Master analysis software.


\paragraph{Adsorbates}
During this study several adsorptive options were investigated. 
The initial choice was nitrogen, as many standard interpretation methods exist for graphitised carbons. 
It is possible to determine absolute surface areas using highly accurate volumetric adsorption equipment as low as approximately 0.5 - 1 m$^{2}$g $^{-1}$ with nitrogen as the adsorptive.
Both surface area and the corresponding pore size information can then be calculated, via application of the commercially available software BELMaster, which is supplied with the instrument.
%ADD MORE DETAIL ABOUT SOFTWARE 
However, results presented in section \ref{N-Results} demonstrated the need for an alternative adsorptive gas to be sourced. 
This was due to inconsistencies between replicate isotherms which were thought to be brought about by the limited sample volumes, as well as the low specific surface areas requiring the instrument to operate at its limit of detection.

Krypton was used as an alternative adsorbate due to its capacity to extend the detection limit of the absolute surface areas down to 0.05 m$^{2}$ g$^{-1}$. 
Due to its extremely low saturated vapour pressure, the number of krypton molecules in the free space of the sample cell are significantly lower when compared to nitrogen or argon at their respective boiling temperatures. 
This leads to the high sensitivity of krypton adsorption at 77.35 K, making it the adsorbate of choice for measuring low surface area solids \cite{Beebe1945}.
High precision krypton adsorption isotherms were collected at 77.35 K for the graphitic samples. 
Stable temperatures were achieved by fully submerging both a blank and sample chamber in a 3 L Dewar, filled with liquid nitrogen. 
At 77.35 K, krypton is approximately 38 K below its triple point (TP = 115.8 K). 
It is commonly accepted throughout the literature that the saturated vapour pressure, $p_{0}$, of supercooled liquid krypton can be adopted for the determination of specific surface areas, where the adsorbed krypton layer is in a liquid-like state.
At pressures close to 0.22 kPa krypton begins to sublimate \cite{Thommes2015}.
Measurements were therefore recorded over the relative pressure range: 10$^{-8}<p/p\textsubscript{0} \leqslant$ 0.65, where $p$ is the pressure of the adsorbing gas in kPa, and $p\textsubscript{0}$ is 0.3310 kPa. 

Lastly, CO$_{2}$ adsorption experiments at 273.15 K and 298.15 were performed on a limited number of samples to investigate the presence of microporosity in the graphite structures.  


\paragraph{Interpretation}
Total specific surface areas ($S$\textsubscript{BET}) were calculated via the standard Brunauer-Emmett-Teller (BET) model \cite{Brunauer1938_34}, which incorporates multi-layer adsorption. 
A relative pressure range 0.05 - 0.3 was utilized to deduce the surface area, as at higher values of $p/p_{0}$ capillary condensation effects alter the linearity of the BET plot. 


A method for interpreting the void size information for krypton on a smooth graphite surface, modelled with slit geometry was developed during this study as commercial void size interpretations for krypton adsorption were unavailable. 
The GCMC model, described in section \ref{sub:3-Gasadsorptionmodifications}, was first validated for GCB, and then demonstrated on the IG-graphites and an activated carbon sample. 
The simulation was then applied to the Gilsocarbon graphite samples to provide PSDs that covered the micropore/mesopore range. 
Results for all samples are presented below.
The increased presence of porosity throughout the structure, caused by oxidative and irradiative process, will increase the internal surface area of the graphite, which could reach values of 1 m$^{2}$g$^{-1}$ in high mass loss material.
This value is still very small compared to many other samples analysed using this technique and therefore required the application of a highly sensitive  approach in order to accurately characterise the changes in surface area.


\subsection{Sample Preparation} 

\section{Results}

\begin{figure}[H]
	\centering
	\subfloat[]
	{\label{fig:IG110irradiated}
		\includegraphics[width=0.49\linewidth]{./Figures/IG-110/Virgin/Virgin_IG110}}
	\subfloat[]
	{\label{fig:IG430irradiated}
		\includegraphics[width=0.49\linewidth]{./Figures/IG-110/irradiated/IG110_Irradiated.png}}
	\caption[]
	{Representative experimental isotherms for: 
		(a) virgin IG-110, 
		(b) irradiated IG-110 showing increased levels of received dose.}
	\label{fig:TORT}
\end{figure}


Figures/IG-110/irradiated

However, for the irradiated graphite specimens, similarly to the activated carbon sample, there is a noticeable difference between the simulated, stepwise isotherms and the experimental, smoother Kr adsorption isotherms observed over the entire pressure range.
The reasoning for the difference in the shape of the isotherms is discussed in detail in section \ref{IrrGraphIso}; it is likely that the non-uniformities in the graphite surface, introduced most likely by irradiation damage, led to the production of a smoother, non-stepped adsorption isotherm, which cannot be reproduced by the GCMC Kr model. 
In order for the simulation to produce model isotherms calculated by the GCMC  Kr simulation to display a reduction in the stepwise formation of the isotherms, the current Kr kernel would need to be heavily modified, taking in to account the roughness of the graphite's surface.
Due to the time constraints of the project, these amendments could not be completed within the time scale, and instead are discussed in the \textit{future work} section of this thesis \ref{InvestigateSteps}. 

It can be seen in Figures \ref{fig:GCMCM899} - \ref{fig:GCMC3M44} that when fitting the experimental data of the irradiated graphite specimens the simulation again overestimates the initial gas adsorption step at low pressure; where this difference is particularly noticeable in low weight loss specimens due to the very sloped first step.
The simulated curve then progresses horizontally until it intersects the experimental curve, where the fit of the data improves. 
The match between the model and experimental isotherm at the point of the second step causes a further deviation in the data, although the difference between the two models is small.
Additionally, the simulation fails to predict the onset of the third step, at pressures larger than 170 Pa, due to the capillary condensation effect discussed previously.

For the samples with higher weight loss, such as samples 0M55, 3M44 and M595, the distance between the two curves is improved, where the simulation and the experimental isotherms correctly predict the first step observed at low pressure.
However, as the experimental isotherms do not show the formation of the second and third step, and consequently the simulated curve sits either side of the experimental isotherm as it progresses.
In the highest weight loss sample, M595, the GCMC Kr model is able to closely simulate the adsorption isotherm over the entire range of pressures. 

The overall distribution of the pores predicted by the GCMC Kr are similar for the entire irradiated graphite specimens. 
However, the volume contribution of the smallest pores increases in the higher mass loss specimens highlighted by the increasing total cumulative volume plots.
This suggests that in samples of increased mass loss and higher cumulative dose the presence of the smallest pores is increased, likely introduced by radiolytic oxidation of the surface.
However, this contribution of pore volume for the pore range 2 - 50 nm is still very small when compared to the total pore volume measured by micropycnometry.

It should be noted that various kinds of microscopy techniques, such as SEM, can be used to access the porosity at these length scales. 
However, these techniques can only examine a very limited field of view, of what is a very disordered material. 
Gas adsorption is capable of probing the entire gas accessible pore space in one measurement. 
Whilst it cannot reveal information on the closed pore volume, such volumes cannot contribute to gas flow path of the material, and therefore should not impact the pore network model which will be used to access the permeability of the oxidised graphites during Chapter \ref{chapter8}.


\section{Discussion}



\section{Conclusions}


The deductions within this work are semi-quantitative.
In further work, computational modelling will be used to generate quantitative void structure models at every oxidation stage.  
Those will be used to make a definitive judgement on the RPV model, and to provide data for inputting to current larger scale models of reactor core behaviour such as FEAT-DIFFUSE.

\clearpage
 
\section*{Acknowledgements}

The financial support and advice of EDF Energy (Barnwood, Gloucester, UK) is gratefully acknowledged. However, the views expressed in this paper are those of the authors and do not necessarily represent the views of the sponsors. 


\section*{Appendix A. Supplementary materials }

Supplementary data associated with this article can be found in the on-line version at ...

\section*{References}

\bibliographystyle{elsarticle-num-names}
\bibliography{Jones_2021_GA_Irradiated_graphite}

\end{document}
